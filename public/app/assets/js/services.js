angular.module('logIn', ['ngResource', 'globalModule'])
    .factory('getProfile', function($resource){
        return $resource('http://lockedinapi.herokuapp.com/users/all', {} , { 'query' : { method : 'get', isArray : false}});
    })
    .service('logInService', ['$http', function ($http) {
        this.logInData = function (user) {
            return $http.post('http://lockedinapi.herokuapp.com/user' , user);
        };
    }])
    .service('userService', ['$http', function ($http) {
        this.getUsersData = function (offset) {
            return $http.get('http://lockedinapi.herokuapp.com/users/all/' + offset);
        };
    }])
    .service('dataService', ['$http', function ($http) {
        this.sendShoutOut = function (params) {
            return $http.post('http://lockedinapi.herokuapp.com/shoutout', params);
        };
    }])
    .service('recievedService', ['$http', function ($http) {
        this.getSendersOfShoutouts = function (params) {
            return $http.get('http://lockedinapi.herokuapp.com/users/recieved/' + params);
        };
    }])
    .service('profileService', ['$http', function ($http) {
        this.updatePic = function (pic) {
            return $http.put('http://lockedinapi.herokuapp.com/user', pic);
        };
    }])
    .service('countService', ['$http', function ($http) {
        this.countUsers = function () {
            return $http.get('http://lockedinapi.herokuapp.com/users/number');
        };
    }])
    .service('leaveService', ['$http', function ($http) {
        this.leaveApp = function () {
            return $http.post('http://lockedinapi.herokuapp.com/user/logout');
        };
    }])
    .service('myShoutsService', ['$http', function ($http){
        this.getShouts = function() {
          return $http.get('http://lockedinapi.herokuapp.com/shoutout');
        }
    }])
    .service('deleteUserService', ['$http', function ($http) {
        this.del = function () {
            return $http.delete('http://lockedinapi.herokuapp.com/user/');
        };
    }]);
   