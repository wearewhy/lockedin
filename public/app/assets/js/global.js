angular.module('globalModule', [ 'ngTouch'])
    .controller('GlobalCtrl', ['$scope','$http','$location','$timeout','$rootScope','leaveService','logInService','recievedService','deleteUserService',
        function($scope, $http, $location, $timeout, $rootScope, leaveService, logInService, recievedService, deleteUserService) {
            
            /*
            **
            **  GLOBAL AND STARTUP VARIABLES
            **  
            */
            $scope.user = {
                facebook : null,
                 twitter : null,
               instagram : null,
             preferences : {},
                   token : null,
            };
            $scope.loginNetwork;
            $scope.name;
            $scope.shoutOutIndex;
            $scope.termsAccepted = false;
            $scope.notificationsCentre = 'assets/partials/notificationsCentre.html';
            $scope.helpCentre = 'assets/partials/helpCentre.html';
            $scope.header = 'assets/partials/header.html';
            $scope.listenerFbMusicTemplate = 'assets/partials/listenerFbMusic.html';
            $scope.listenerInstagram = 'assets/partials/listenerInstagram.html'
            $scope.headerBool = null;
            $scope.count;
            $scope.shoutOutsRecieved = [], $scope.djShoutOutsRecieved = [];
            var clickBool = false;

            /*
            **
            **  OPEN AND CLOSE NAVIGATION
            **  
            */
            $scope.toggleNav = function(event){
                $('.top-nav').toggleClass('active');
            }

            /*
            **
            **  OPEN AND CLOSE About view
            **  
            */
            $scope.about = function(){
                $('.top-nav').toggleClass('active');
                $location.path("/about");
            }

            /*
            **
            **  CHECK SOCIAL LOG IN STATUS
            **  
            */
            var online = function(session){
                var current_time = (new Date()).getTime() / 1000;
                return session && session.access_token && session.expires > current_time;
            };
    
            var fb = hello( "facebook" ).getAuthResponse();
            var tw = hello( "twitter" ).getAuthResponse();
            if(tw === null && fb === null){
                $location.path("/login");
            }
            
            /*
            **
            **  Check whether this is a new profile or an existing user
            **  
            */
            $scope.getProfile = function(user){
                $scope.user = user;
                $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
                var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                    }
                };
                $http.get('http://lockedinapi.herokuapp.com/user', config)
                .success(function(data){
                    $timeout(function() {
                        $scope.user = data['data'];
                        $scope.user['token'] = data['data']['access_token'];
                        $scope.user['peer'] = $scope.user.shoutouts_r.length;
                        if($scope.user['twitter'] != null){
                            $scope.user['twitter']['profile_image_url'] =  $scope.user['twitter']['profile_image_url'].replace(/_normal/g, "");
                        }
                        $rootScope.$broadcast('userUpdated', $scope.user);
                        $location.path("/profile");
                        $scope.getShoutOutSenders();
                    }); 
                })
                .error(function(data, status){
                    console.log(status);
                    if (status == 401) {
                        var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                            }
                        };
                        $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.user, config)
                        .success(function(data){
                            $scope.user['token'] = data['token'];
                            $scope.getProfile($scope.user);
                        })
                        .error(function(data){
                            console.log(data);
                        });
                    }
                    if(status == 400){
                        $location.path("/login");
                    }
                }); 
            }

            /*
            **
            **  Send login credentials and initial social media data to server
            **  
            */
            $scope.sendUser = function(user, profileSize){
                logInService.logInData(user)  
                .success(function(data){
                    $scope.user['token'] = data.token;
                    $scope.user['id'] = data['_id'];
                    $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
                    $scope.getProfile($scope.user);
                })
                .error(function(data){
                    console.log(data);
                });
            }

            /*
            **
            **  This function is used to add second or third social media accounts to an existing profile
            **  
            */
            $scope.mergeAccounts = function(user){
                $http.post('http://lockedinapi.herokuapp.com/user/merge', user)
                .success(function(data){
                    $timeout(function() {
                        $location.path("/profile");
                        $scope.$apply();
                    },400);
                })
                .error(function(data){
                    console.log(data);
                });
            }

            /*
            **
            **  Oauth variable
            **  We are using Hello.js to bring all of the logins under one set of credentials
            */
            hello.init({
                facebook : '244491825745560',
                 twitter : 'LweKMQHRSVY0Wqbxt6uvGiyVF',
               instagram : '853236a03e36460a882be48ca895a0a6',
                  google : '862506358537-1rkstooaht2ao5gig6llug776v3m0alf.apps.googleusercontent.com'
             },{
                oauth_proxy : 'https://auth-server.herokuapp.com/proxy'
            });

            /*
            **
            **  Initial login and profile creation
            **  
            */
            $scope.login = function(network, profileSize){
                $('.social-loading').css({'display':'block'});
                if(profileSize === 1) {
                    $scope.loginNetwork = network;
                }
                hello.login( network, {scope:'friends, basic, user_likes'}, function(e){
                    hello( network ).api("me").success(function(json){
                        $scope.user['social'] = network;
                        $scope.user[network] = json;
                        $scope.name = $scope.user[network].name;
                        $scope.$apply();
                        if(network === 'facebook'){
                            $scope.user[network].picture =  $scope.user[network].picture + '?type=large';
                            hello( network ).api(network+':me/music').success(function(json){
                                if(json){
                                    $scope.user.facebook['music'] = json;
                                    if(profileSize === 1){
                                        $http.defaults.headers.common.Authorization= null;
                                        $scope.user['token'] = null;
                                        $scope.sendUser($scope.user, profileSize);
                                    }else{
                                       $scope.mergeAccounts($scope.user);
                                    } 
                                }
                            }).error(function(e){
                                console.log(e);
                            });
                        }
                        else if(network === 'instagram'){
                            hello( network ).api(network+':me/photos').success(function(json){
                                if(json){
                                    $scope.user.instagram['photos'] = json;
                                    if(profileSize === 1){
                                        $http.defaults.headers.common.Authorization= null;
                                        $scope.user['token'] = null;
                                       $scope.sendUser($scope.user, profileSize);
                                    }else{
                                       $scope.mergeAccounts($scope.user);
                                    } 
                                }
                            }).error(function(e){
                                console.log(e);
                            });
                        }
                        else{
                            if(network === 'twitter'){
                                $scope.user.twitter.profile_image_url = $scope.user.twitter['profile_image_url'].replace(/_normal/g, "");
                            }
                            if(profileSize === 1){
                                $http.defaults.headers.common.Authorization= null;
                                $scope.user['token'] = null;
                                $scope.sendUser($scope.user, profileSize);
                            }else{
                               $scope.mergeAccounts($scope.user);
                            } 
                        }
                    }).error(function(){
                        alert("Authentication error. Please try again");
                         $('.social-loading').css({'display':'none'});
                    });
                });   
            }

            /*
            **
            **  Logout of app / network * Twitter offers no network logout
            **  
            */
            $scope.logout = function(){
                leaveService.leaveApp()
                .success(function(result){
                    function logOutSocial(){
                        var fb = hello( "facebook" ).getAuthResponse();
                        var tw = hello( "twitter" ).getAuthResponse();
                        var inst = hello( "instagram" ).getAuthResponse();
                        if(fb != null){
                            hello( "facebook" ).logout({force:true},function(){
                                logOutSocial();
                            });
                        }
                        if(tw != null){
                            hello( "twitter" ).logout(function(){
                                tw.access_token = null;
                                logOutSocial();
                            });
                        }
                        else if(online(inst)){
                            hello( "instagram" ).logout(function(){
                                logOutSocial();
                            });
                        }
                        else {
                            $location.path("/login");
                        }
                    }
                    logOutSocial();
                    
                })
                .error(function(data, status){
                    $location.path("/login");
                    if (status == 401) {
                        var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                            }
                        };
                        $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.user, config)
                        .success(function(data){
                            $scope.user['token'] = data['token'];
                            leaveService.leaveApp();
                        })
                        .error(function(data){
                            console.log(data);
                        });
                    }
                    if(status == 400){
                        $location.path("/login");
                    }
                });
            }
            //  Delete dialog toggle
            $scope.deleteToggle = false;

            /*
            **
            **  Open notifications panel (This is the button in the top left of the screen that displays shout out senders)
            **
            */
            $scope.openNotifications = function(){
                if($scope.user.peer > 0){
                    $('.notification-centre-panel, #js-profile-shoutouts-notification').toggleClass('isActive');
                }
            }

            /*
            **
            **  Get a list of the other users that have sent a shoutout to me,
            **  This is called at the start of the app and also from the profile controller when reloading the view
            */
            $scope.getShoutOutSenders = function(){
                if($scope.shoutOutsRecieved.length < 1 || $scope.shoutOutsRecieved.length < $scope.user.peer){
                    $scope.shoutOutsRecieved = [];
                    $scope.djShoutOutsRecieved = [];
                    recievedService.getSendersOfShoutouts($scope.user._id).then(function(res) {
                        var max;
                        for(var i = 0, max = res.data.data.length; i < max; i++){
                            if(res.data.data[i].preferences.is_dj === true){
                                var max2;
                                for(var d = 0, max2 = $scope.user.shoutouts_r.length; d < max2; d++){
                                    if($scope.user.shoutouts_r[d].who === res.data.data[i]._id && res.data.data[i]['done'] != true){
                                        res.data.data[i]['when'] = $scope.user.shoutouts_r[d].when;
                                        res.data.data[i]['done'] = true;
                                        $scope.djShoutOutsRecieved.push(res.data.data[i]);
                                    }
                                }
                            }else{
                                $scope.shoutOutsRecieved.push(res.data.data[i]);
                            }
                        }
                    }, function(data){
                        if (data.status == 401) {
                            $location.path('/#');
                        }
                    });
                }else {
                    $('.notification-centre-panel, #js-profile-shoutouts-notification').toggleClass('isActive');
                    sorBool = true;
                }    
                
            }
            
            /*
            **
            **  Delete profile - TODO move into angular ng-click event
            **  
            */
            $scope.deleteProfile = function(){
                $scope.deleteToggle = true;
                $rootScope.$broadcast('dialogChange');
            };
            
            $(document).on('click', '.deleteBtn', function(){
                deleteUserService.del()
                    .then(function(){
                        $('.dialog').removeClass('active');
                        logOutSocial();
                        $location.path('/login');
                    }, function(data){
                        alert(data);
                    });
            });
            
            function logOutSocial(){
                var fb = hello( "facebook" ).getAuthResponse();
                var tw = hello( "twitter" ).getAuthResponse();
                var inst = hello( "instagram" ).getAuthResponse();

                if(fb != null){
                    hello( "facebook" ).logout({force:true},function(){
                        logOutSocial();
                    });
                }
                if(tw != null){
                    hello( "twitter" ).logout(function(){
                        tw.access_token = null;
                        logOutSocial();
                    });
                }
                else if(tw != (inst)){
                    hello( "instagram" ).logout(function(){
                        logOutSocial();
                    });
                }
                else {
                    $location.path("/login");
                }
            }
}]);