function ProfileCtrl($scope, $rootScope, $routeParams, $timeout, $location, $parse, profileService, deleteUserService, logInService, myShoutsService, $http){

    /*
    **
    **   Templates and startup variables
    **  
    */
    $scope.$parent.headerBool = true;
    $scope.instagramTemplate = 'assets/partials/instagram.html';
    $scope.fbMusicTemplate = 'assets/partials/fbMusicTemplate.html';
    $scope.addOrLoseText;
    var picBool = true, dialogTimer, timer3;
    $scope.user = $scope.$parent.user;
    if($scope.user['token'] === null ){
        if($scope.user['token'] === null){
            $location.path('/#');
        }else{
            var timer = $timeout (
                function () {
                    $scope.setUpProfile();
                },
                50
            );
        }
    }
    else{
        var timer = $timeout (
            function () {
                $scope.setUpProfile();
            },
            50
        );
    }

    /*
    **
    **   Update Instagram
    **  
    */
    if($scope.user.instagram != 'undefined' && $scope.user.instagram != null) {
        $.ajax({
            url:"https://api.instagram.com/v1/users/" + $scope.user.instagram.id + "/media/recent/?client_id=853236a03e36460a882be48ca895a0a6",
            "method": "GET",
            dataType:'jsonp',
            cache:false,
            success: function(data){
                if(data.meta.code == 400){
                        alert('Error updating instagram images');
                }
                if(data.meta.code == 200){
                        $scope.user.instagram.photos = data;
                        $scope.$parent.user.instagram.photos = data;
                }
            }
        });
    }

    /*
    **
    **   Profile Images object
    **   carousel setup
    */
    $scope.setUpProfile = function(){
        if(typeof($owls) != 'undefined' || $('.profile-swipe-container').hasClass('owl-carousel')){
            if($owls != null){
                $owls.trigger ('destroy.owl.carousel');
                $('.owl-item').unwrap();
                $('.profile-swipe-container').removeClass('owl-carousel owl-loaded owl-theme');
            }
        }
        $scope.profilePics = [];
        $scope.picPref;
        if(typeof($scope.user.facebook) != 'undefined' && $scope.user.facebook != null){
            $scope.profilePics.push($scope.user.facebook.picture);
        }
        if(typeof($scope.user.twitter) != 'undefined' && $scope.user.twitter != null){
            $scope.profilePics.push($scope.user.twitter.profile_image_url);
        }
        if(typeof($scope.user.instagram) != 'undefined' && $scope.user.instagram != null){
            $scope.profilePics.push($scope.user.instagram.data.profile_picture);
        }
        for (var i = 0; i < $scope.profilePics.length; i++){
            if ($scope.profilePics[i] === $scope.user.preferences.image){
                $scope.picPref = i;
            }
        }
        
        var timerUP = $timeout (
            function () {
                if($scope.profilePics.length > 1 && picBool === true){
                    picBool = false;
                    $owls = $('.profile-pics');
                    $timeout(function(){
                        $owls.owlCarousel({
                            items : 1,
                            margin : 0,
                            dots : false,
                            startPosition : $scope.picPref,
                            loop : false
                        });
                    },400); 
                }else{
                    $owls = $('.profile-pics');
                    $owls.owlCarousel({
                        items : 1,
                        margin : 0,
                        dots : false,
                    });
                }
            },300);
    };

    /*
    **
    **   watch for new image and update carousel
    **  
    */
    $scope.$on('userUpdated', function() {        $scope.setUpProfile();
    });
    
    /*
    **
    **   Warning dialog
    **  
    */
    $scope.lockIn = function(){
        if($scope.termsAccepted === false){
            $scope.$parent.deleteToggle = false;
            $rootScope.$broadcast('dialogChange');
            var dialogTimer = $timeout(function(){
                $('.dialog').addClass('active');
            },200);
            $scope.user.preferences.image = $('.owl-item.active').children().children('.profile-pic').data('img');
        }
        else {
            $location.path('/lockedin');
        }
    };

    /*
    **
    **    Remove network from profile
    **    This is triggered by a click event on the social network buttons in the profile view
    */
    $scope.remove = function(network) {
        $scope.user[network] = null;
        $scope.$parent.user[network] = null;
        profileService.updatePic($scope.user)
        .then(function(data){
        }, function(data){
            alert('Error : Could not update profile');
        });
    };

    /*
    **
    **    Destroy timeouts
    **    
    */
    $scope.$on('$destroy', function(event){
        if(timer){
            $timeout.cancel( timer );
        }
        if(dialogTimer){
            $timeout.cancel( dialogTimer );
        }
        if(timer3){
            $timeout.cancel( timer3 );
        }
    });

    /*
    **
    **    CHECK SHOUTOUTS AND NOTIFY
    **    
    */
    myShoutsService.getShouts()
        .success(function(d){
            if(d['data']['shoutouts_r'].length > $scope.$parent.user.shoutouts_r.length){
                $scope.$parent.user.shoutouts_r = d['data']['shoutouts_r'];
                $scope.$parent.user.peer = d['data']['shoutouts_r'].length;
                $scope.notifyMe(d['data']['shoutouts_r'].length);
                $scope.$parent.getShoutOutSenders();
            }
        })
        .error(function(d){
            var config = {headers:  {
                'Authorization' : 'Bearer ' + $scope.$parent.user['token']
            }};
            $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.$parent.user, config)
            .success(function(data){
                $scope.$parent.user['token'] = data['token'];
                $scope.getProfile($scope.$parent.user);
            })
            .error(function(data){
                console.log(data);
            });
        });

        $scope.notifyMe = function(number){
            $('.profile-shoutouts-notification-inner').addClass('blink');
            $('#js-nav-footer-count').html(number);
            $timeout(function(){
                $('.profile-shoutouts-notification-inner').removeClass('blink');
            },4000);
    }

    /*
    **
    **    This function opens and closes the social network add/remove modal window
    **    
    */
    $scope.addItOrLoseItModal = function(network){
        $('.add-or-lose-modal').removeClass('twitter instagram facebook');
        $('.add-or-lose-modal').addClass(network);
        $('.add-or-lose-modal').data('network',network );
        if(network === $scope.loginNetwork){
            $('.add-or-lose-modal').addClass('add-or-lose-modal-void');
            $scope.addOrLoseText = 'Unable to remove ' + network;
        }else{
            $('.add-or-lose-modal').removeClass('add-or-lose-modal-void');
             if($scope.$parent.user[network] != null && $scope.$parent.user[network] != ''){
                $scope.addOrLoseText = 'Remove ' + network;
            }
            else{
                $scope.addOrLoseText = 'Add ' + network;
            }
        }
        $('.add-or-lose-modal').toggleClass('isVis');
    }

    /*
    **
    **    Remove network
    **    
    */
    $scope.addItOrLoseIt = function (){
        var network = $('.add-or-lose-modal').data('network');
        if(network === $scope.loginNetwork){

        }else{
            if($scope.$parent.user[network] != null && $scope.$parent.user[network] != ''){
                $scope.remove(network);
            }
            else{
                $scope.$parent.login(network, 2);
                $('.add-or-lose-modal').removeClass('isVis');
            }
        }
    }
}