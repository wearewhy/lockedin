function LoginCtrl($scope, $routeParams, $location, countService){
     $scope.$parent.headerBool = null;
    //
    //  Get number of listeners currently locked in.
    //
    countService.countUsers().then(function(result){
        $scope.$parent.count = result.data.data;
        $('.social-loading').removeAttr('style');
    });
    var loginBool = false;
    
    
    (function loginBg(){
        $intro = $('#js-login-main');
        if($intro != 'undefined' && loginBool === false){
            var bg = $intro.data('bg');
            var im = new Image();
            im.src = bg;
            $intro.css({ 'backgroundImage' : "url("+im.src+")"});
            loginBool = true;
            setTimeout(function(){
                $intro.css({'opacity' : 1});
            },400);
        }else {
            $intro = $('#js-login-main');
            loginBg();
        }
        
    })();
}