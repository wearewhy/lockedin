function ProfileCtrl($scope, $rootScope, $routeParams, $timeout, $location, $parse,logInData, localStorageService, profileService, socket, deleteUserService){
    
    //
    //  Templates
    //
    $scope.instagramTemplate = 'assets/partials/instagram.html';
    $scope.fbMusicTemplate = 'assets/partials/fbMusicTemplate.html';
    var picBool = true, dialogTimer, timer3;
    
    if($scope.user['token'] === null ){
        $scope.$parent.user = localStorageService.get('user');
        $scope.user = localStorageService.get('user');
        $scope.$parent.getProfile($scope.user);
        if($scope.user['token'] === null){
            $location.path('/#');
        }else{
            // $scope.profile = localStorageService.get('profile');
            // $scope.confirmation = localStorageService.get('confirmation');
            // $scope.networks = $scope.profile.user.profile;
            var timer = $timeout (
                function () {
                    $scope.setUpProfile();
                },
                50
            );
        }
    }
    else{
        var timer = $timeout (
            function () {
                $scope.setUpProfile();
            },
            50
        );
    }

    //
    // Update Instagram
    //
    if($scope.user.instagram != 'undefined' && $scope.user.instagram != null) {
        $.ajax({
            url:"https://api.instagram.com/v1/users/" + $scope.user.instagram.id + "/media/recent/?client_id=853236a03e36460a882be48ca895a0a6",
            "method": "GET",
            dataType:'jsonp',
            cache:false,
            success: function(data){
                if(data.meta.code == 400){
                        alert('Error updating instagram images');
                }
                if(data.meta.code == 200){
                        $scope.user.instagram.photos = data;
                        $scope.$parent.user.instagram.photos = data;
                }
            }
        });
    }

    //
    //  Profile Images object
    //
    $scope.setUpProfile = function(){
        if(typeof($owls) != 'undefined' || $('.profile-swipe-container').hasClass('owl-carousel')){
            $owls.trigger ('destroy.owl.carousel');
            $('.owl-item').unwrap();
            $('.profile-swipe-container').removeClass('owl-carousel owl-loaded owl-theme');
        }
        $scope.profilePics = [];
        $scope.picPref;
        if(typeof($scope.user.facebook) != 'undefined'){
            $scope.profilePics.push($scope.user.facebook.picture);
        }
        if(typeof($scope.user.twitter) != 'undefined'){
            $scope.profilePics.push($scope.user.twitter.profile_image_url);
        }
        if(typeof($scope.user.instagram) != 'undefined'){
            $scope.profilePics.push($scope.user.instagram.data.profile_picture);
        }
        for (var i = 0; i < $scope.profilePics.length; i++){
            if ($scope.profilePics[i] === $scope.user.preferences.image){
                $scope.picPref = i;
            }
        }
        // if(typeof($scope.picPref) === 'undefined'){
        //     $scope.picPref = 1;
        // }
        
        //$scope.picPref = $scope.picPref +=1;
        var timerUP = $timeout (
            function () {
                if($scope.profilePics.length > 1 && picBool === true){
                    picBool = false;
                    $owls = $('.profile-pics');
                    $timeout(function(){
                        $owls.owlCarousel({
                            items : 1,
                            margin : 0,
                            dots : false,
                            startPosition : $scope.picPref,
                            loop : true
                        });

                    },400); 
                }else{
                    $owls = $('.profile-pics');
                    $owls.owlCarousel({
                        items : 1,
                        margin : 0,
                        dots : false,
                    });
                }
            },
            300
        );
    }

    //
    //  watch for new image and update carousel
    //
    $scope.$on('userUpdated', function() {        $scope.setUpProfile();
    });
    
    //
    //  Warning Dialog
    //
    $scope.lockIn = function(){
        $scope.$parent.deleteToggle = false;
        $rootScope.$broadcast('dialogChange');
        var dialogTimer = $timeout(function(){
            $('.dialog').addClass('active');
        },200);
        $scope.user.preferences.image = $('.owl-item.active').children().children('.profile-pic').data('img');
    } 
    $(document).on('click', '.negative', function(){
       $('.dialog').removeClass('active');
    });
    $(document).on('click', '.true', function(){
        profileService.updatePic($scope.user).then(function(result) {
            var timer3 = $timeout (
                function() {
                    localStorageService.clearAll();
                    window.localStorage.clear();
                    localStorageService.cookie.remove('user');
                    localStorageService.set('user', $scope.user);
                    $location.path('/lockedin');
                }, 
            30);
        });
    });

    //
    //  Remove network from profile
    //
    $scope.remove = function(network) {
        $scope.user[network] = null;
        $scope.$parent.user[network] = null;
    }

    //
    //  Delete profile
    //
    $scope.deleteProfile = function(){
        $scope.$parent.deleteToggle = true;
        $rootScope.$broadcast('dialogChange');
    }
    $(document).on('click', '.deleteBtn', function(){
        deleteUserService.del()
            .success(function(data){
                console.log(data);
            })
            .then(function(){
                $('.dialog').removeClass('active');
                $location.path('/login');
            });
    });
   


    $scope.$on('$destroy', function(event){
        $timeout.cancel( timer );
        if(dialogTimer){
            $timeout.cancel( dialogTimer );
        }
        $timeout.cancel( timer3 );
    });

    //
    //  save Socket ID
    //
    socket.on('identification', function (data) {
        //console.log(data.data);
        $scope.user.preferences['socketioID'] = data.data;
        //console.log($scope.$parent.user);
    //     console.log($scope.user);
    //     profileService.updatePic($scope.user).then(function(result) {
    //         localStorageService.set('user',$scope.user);
    //     }, function(err){
    //         console.log(err);
    //     });
    });
}