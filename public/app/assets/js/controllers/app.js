angular.module('AppCtrl', ['ngRoute', 'logIn', 'globalModule', 'ngTouch'])
    .config(siteRouter)
    .directive('stopEvent', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('click', function (e) {
                e.stopPropagation();
            });
        }
    };
 });

function siteRouter($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl : 'assets/partials/home.html', 
        })
         .when('/login', {
            templateUrl : 'assets/partials/login.html', 
            controller : LoginCtrl
        })
        .when('/loginform/:social', {
            templateUrl : 'assets/partials/loginform.html', 
            controller : LoginCtrl
        })
        .when('/profile', {
            templateUrl : 'assets/partials/profile.html', 
            controller : ProfileCtrl
        })
        .when('/lockedin', {
            templateUrl : 'assets/partials/lockedin.html', 
             controller : LockedinCtrl
        })
        .when('/about', {
            templateUrl : 'assets/partials/helpCentre.html', 
        })
        .when('/profiles', {
            templateUrl : 'assets/partials/listener.html', 
             controller : LockedinCtrl
        })
        .when('/profiles/:id', {
            templateUrl : 'assets/partials/listener.html', 
             controller : LockedinCtrl
        })
         .otherwise({
            redirectTo : '/'
        });
}