function dialogCtrl($scope, $routeParams, $location, dataService, $timeout){
    $scope.location = $location.path();
    var dialogTimer;
    $scope.$on('dialogChange', function(){
        if($scope.$parent.deleteToggle == true){
            $scope.dialogBox = 'assets/partials/delete.html';
            var dialogTimer = $timeout(function(){
                 $('.dialog').addClass('active');
             },200);
        }else {
            $scope.dialogBox = 'assets/partials/warning.html';
        }
    });

    $scope.$on('$routeChangeStart', function() {
        $('.top-nav').removeClass('active');
        $('.notification-centre-panel, #js-profile-shoutouts-notification').removeClass('isActive');
        setTimeout(function(){
            $('.dialog').removeClass('active');
        },400);
        setTimeout(function(){
            $scope.location = $location.path();
            switch($scope.location){
                case '/profile' : 
                $scope.dialogBox =  'assets/partials/warning.html';
                break;
                case '/lockedin' : 
                $scope.dialogBox = 'assets/partials/shoutout.html';
                break;
            }
        }, 800);
        
    });

    $scope.getLockedIn = function(){
        $scope.$parent.termsAccepted = true;
        $location.path('/lockedin');
    }
    $scope.declineLockin = function(){
        $('.dialog').removeClass('active');
    }
    $scope.notifyMe = function(){
        $('.nav-notification-inner').addClass('blink');
    }
    $scope.shoutOut = function(number){
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes();
        //console.log(datetime);
        params = {
          "id": number,
       "when" : datetime
        }
        dataService.sendShoutOut(params).then(function(res) {
            $('.dialog').removeClass('active');
            $scope.$emit('dialogEvent');
        }, function(){
            alert('There was an error sending your shoutout, please try again later');
        });
    }
      $scope.$on('$destroy', function(event){
        $timeout.cancel( dialogTimer );
    });


}