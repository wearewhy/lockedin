function LockedinCtrl($scope, $http, userService, $routeParams, $location, profileService, myShoutsService, $timeout){
        
    /*
    **
    **   Startup variables
    **  
    */
    $scope.listeners = null;
    $scope.shoutOutSend;
    $scope.shoutOutIndex;
    $owls = null;
    var endBool = false;
    $scope.individulals = [];
    $scope.currentListsner = 0;
    $scope.viewProfileMessage = "View profile";
    var ofs = 1, swipeBool = false, owlBuild, owlTimer, loadTimer, lastUser = '000', thisUser, alerted = false, pars, helpBool = false, countOverlay = false, profileVisBool = false;

    /*
    **
    **   Toggle for displaying profiles or hiding carousel
    **  
    */
    $scope.showLockedInProfile = function(){
        $('.listeners').toggleClass('hidden');
        $('.lockedin-profile-btn-inner-arrow').toggleClass('active');
        if(profileVisBool === false){
            $scope.viewProfileMessage = "Close profile";
            $('.notification-centre-panel, #js-profile-shoutouts-notification').removeClass('isActive');
            profileVisBool = true;
            $(".listeners").one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', 
            function() {
                $('#js-lockedin-profile-single').addClass('isActive');
            });
            setTimeout(function(){
                $("#js-lockedin-profile-single").addClass('isVis');
            },300);

        }else {
            profileVisBool = false;
            $scope.viewProfileMessage = "View profile";
            $("#js-lockedin-profile-single").removeClass('isVis');
            setTimeout(function(){
                $("#js-lockedin-profile-single").removeClass('isActive');
            },300);
        }
    }

    /*
    **
    **   Update current lockedin model
    **  
    */
    $scope.currentListsnerUpdate = function(){
        $scope.currentListsner = $('.owl-item.active .profile').data('index');
        $scope.$apply();
    }

    /*
    **
    **   Get profile from local storage or re-direct
    **  
    */
    if($scope.user['token'] === null ){
        $http.defaults.headers.common.Authorization='Bearer ' + $scope.$parent.user['token'];
        $scope.$parent.getProfile($scope.$parent.user);
        if($scope.$parent.user['token'] === null){
            $location.path('/#');
        }
    }

    /*
    **
    **   Update Profile before pulling in users
    **  
    */
    $scope.updateMyProfile = function(user){
        profileService.updatePic(user)
        .then(function(data){
            //  First call to db for listeners
            ofs = 1;
            $scope.getSomeListeners(ofs);
        }, function(data){
            alert('Error : Could not update profile');
        });
    }

    /*
    **
    **   Help overlay
    **  
    */
    $scope.getHelp = function(){
        if(helpBool === false){
            if(sorBool === true){
                $('.notification-centre-panel, .nav-notification-centre-btn').removeClass('isActive');
                sorBool = false;
            }
            $('#js-help-centre-panel').addClass('isActive');
            helpBool = true;
        }else{
            $('#js-help-centre-panel').removeClass('isActive');
            helpBool = false;
        }
    }

    /*
    **
    **   Get listeners
    **  
    */
    $scope.current = 0;
    $scope.getSomeListeners = function(ofs){
        userService.getUsersData(ofs).then(function(result) {
            var max;
            for(var i = 0, max = result.data.data.length; i < max; i++){
                if(result.data.data[i]['_id'] != $scope.$parent.user['_id']){
                    $scope.individulals.push(result.data.data[i]);
                }
            }
            for(var i = 0; i < $scope.individulals.length; i++){
                if($scope.checkShoutsSent($scope.individulals[i]['_id'])){
                    $scope.individulals[i]['shouted'] = 'true';
                }
            }
            $scope.setUpSwipe(0);
        }, function(data, status){
            console.log(data.status);
                if (data.status == 401) {
                    $location.path('/#');
                }
            });
        }

    /*
    **
    **   Check whether the user has already been shouted out
    **  
    */
    $scope.checkShoutsSent = function(id){
        for(var i = 0; i < $scope.$parent.user.shoutouts_s.length; i++){
            if(id == $scope.$parent.user.shoutouts_s[i]){
                return true;
            }
        }
    }

    /*
    **
    **   Add more listeners
    **  
    */
    $scope.getMoreListeners = function(a , ap){
        a++;
        ofs = a;
        userService.getUsersData(ofs)
        .then(function(r) {
            if(r.data.data.length === 0 && endBool == false){
                $('#js-end-dialog').addClass('active');
                endBool = true;
            }else{
                for(var i = 0; i < r.data.data.length; i++){
                    if(r.data.data[i]['_id'] != $scope.user['_id']){
                        $scope.individulals.push(r.data.data[i]);
                    }
                    if($scope.checkShoutsSent(r.data.data[i]['_id'])){
                        r.data.data[i]['shouted'] = 'true';
                    }
                }
            }
            $scope.setUpSwipe(ap);
        },function(data){
                if (data.status == 401) {
                    $location.path('/#');
                }
        });
    }

    /*
    **
    **   Shout out functionality
    **  
    */
    $scope.checkShout = function(number , index){
        $scope.$parent.shoutOutIndex = $scope.individulals[index];
        $timeout(function() {
            $scope.$apply();
            $('.dialog').addClass('active');
        },50);
    }

    /*
    **
    **   Increases shoutout count once sent
    **  
    */
    $scope.$parent.$on('dialogEvent', function(event) { 
        $scope.individulals[$scope.currentListsner].peer += 1;
    });

    /*
    **
    **   SWIPES BETWEEN PROFILES
    **  
    */
    $scope.setUpSwipe = function(ap){

        for(var i = 0; i < $scope.individulals.length; i ++){
              $scope.individulals[i]['peer'] = $scope.individulals[i].shoutouts_r.length;
        }
        setTimeout(function(){
            swipeBool = false;   
        }, 4000);
        
        /*
        **
        **   Build Carousel
        **  
        */
        owlBuild = $timeout (
            function () {
                $owls = $('.listeners-profile');
                $owls.owlCarousel({
                    items : 1,
                    margin : 0,
                    autoHeight : false,
                    dots : false,
                    callbacks : true,
                    info : true,
                    startPosition : ap,
                });
                
                /*
                **
                **   Remove preloader and 0 opacity after rebuilding the carousel
                **  
                */
                loadTimer = $timeout(function(){
                    $('.listeners-profile').removeClass('isHidden');
                    $('.listener-count').removeClass('loading');
                    $('.listeners-preloader').remove();
                    $('.loading-hint').remove();
                },1500);
                $owls.on('translated.owl.carousel', function(){
                    $scope.currentListsnerUpdate();
                });

                /*
                **
                **   Check whether we are at the end of the carousel and then get more results
                **  
                */
                $owls.on('dragged.owl.carousel', function(e){
                    if (endBool == false){
                        var addPoss = e.item.index;
                       
                        //console.log($scope.individulals[addPoss]);
                        var countPos = e.item.count;
                        if(addPoss === (countPos - 1) &&  swipeBool === false){
                            swipeBool = true;
                            thisUser = $('.owl-item.active .profile').data('me');
                            if(thisUser != lastUser){
                                $('.listeners-profile').addClass('isHidden');
                                $('.listeners').append('<div class="listeners-preloader"><div class="preloader"></div></div>');
                                lastUser = $('.profile:last-of-type').data('me');
                                owlTimer = $timeout(function(){
                                    $owls.trigger ('destroy.owl.carousel');
                                    $('.listener-profile-li').unwrap();
                                    $('.listeners-profile').removeClass('owl-carousel owl-loaded owl-theme');
                                    
                                    $scope.getMoreListeners(ofs, addPoss);
                                    
                                },300);
                            }else {
                                if(alerted === false){
                                    alerted = true;
                                }
                            }
                        }   
                    }
                    
                });
            },
            50
        );
        
        myShoutsService.getShouts()
            .success(function(d){
                if(d['data']['shoutouts_r'].length > $scope.$parent.user.shoutouts_r.length){
                    $scope.$parent.user.shoutouts_r = d['data']['shoutouts_r'];
                    $scope.$parent.user.peer = d['data']['shoutouts_r'].length;
                    $scope.notifyMe(d['data']['shoutouts_r'].length);
                }
            })
            .error(function(data){
                if (data.status == 401) {
                    $location.path('/#');
                }
            });

        $scope.notifyMe = function(number){
            $('.profile-shoutouts-notification-inner').addClass('blink');
            $('#js-nav-footer-count').html(number);
            $timeout(function(){
                $('.profile-shoutouts-notification-inner').removeClass('blink');
            },4000);
        }
    }
    $scope.closeEndDialog = function(){
        $('#js-end-dialog').removeClass('active');
    }
    $scope.$on('$destroy', function(event){
        if(typeof $owls === 'undefined' && $owls != null){
            $owls.trigger ('destroy.owl.carousel');
        }
        if(typeof owlTimer === 'undefined'){
            $timeout.cancel( owlTimer );
        }
        if(typeof loadTimer === 'undefined'){
            $timeout.cancel( loadTimer );
        }
        if(typeof owlBuild === 'undefined'){
            $timeout.cancel( owlBuild );
        }
    });

    //
    //  First function to fire !!!
    //
    $scope.updateMyProfile();
}