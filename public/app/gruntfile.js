module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      dist: [
        'www-deploy/',
      ]
    },
    recess: {
      dist: {
        options: {
          compile: true,
          compress: true
        },
        files: {
          'assets/css/main.css': [
            'assets/less/main.less'
          ]
        }
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
           'assets/less/*/**.less'
        ],
        tasks: ['recess']
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-recess');

  grunt.registerTask('dev', [
    'watch'
  ]);

};