function navCtrl($scope, $location, leaveService) {

    //
    //  Back to settings button
    //
    $scope.setting = function(){
        $location.path('/profile');
    }
    //
    //  Lock out of app
    //
    $scope.lockOut = function(){
        leaveService.leaveApp().then(function(result){
            $location.path('/#');
        },function(data){
            console.log(data);
        });
    }
    

}