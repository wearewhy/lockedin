function LockedinCtrl($scope, $http, userService, dataService, countService, $routeParams, $location, profileService, myShoutsService, $timeout, recievedService){
    
    $scope.listeners = null;
    $scope.shoutOutSend;
    $scope.shoutOutIndex;
    $owls = null;
    $scope.individulals = [];
    $scope.shoutOutsRecieved = [];
    var ofs = 1;
    var owlBuild, owlTimer, loadTimer, lastUser = '000', thisUser, alerted = false, pars, helpBool = false, sorBool = false;;
    
    //  Remove Listener Count Overlay
    $scope.removeListenerCount = function(){
        var element = document.getElementById("js-listener-count");
        element.parentNode.removeChild(element);
    }

    //  Get profile from local storage or re-direct
    if($scope.user['token'] === null ){
        $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
        $scope.$parent.getProfile($scope.$parent.user);
        if($scope.user['token'] === null){
            $location.path('/#');
        }
    }

    //  Update Profile before pulling in users
    $scope.updateMyProfile = function(){
        profileService.updatePic($scope.user)
        .then(function(data){
            //  First call to db for listeners
            ofs = 1;
            $scope.getSomeListeners(ofs);
        }, function(data){
            alert('Error : Could not update profile');
        });
    }

    //  Count lockedIn users
    countService.countUsers().then(function(result){
        $scope.count = result.data.data;
    });

    // Help overlay
    $scope.getHelp = function(){
        if(helpBool === false){
            console.log('click');
            if(sorBool === true){
                $('.notification-centre-panel, .nav-notification-centre-btn').removeClass('isActive');
                sorBool = false;
            }
            $('#js-help-centre-panel').addClass('isActive');
            helpBool = true;
        }else{
            $('#js-help-centre-panel').removeClass('isActive');
            helpBool = false;
        }
    }

    // Get list of people who have sent user a shoutout
    $scope.getShoutOutSenders = function(){
        if(sorBool === false){
            if(helpBool === true){
                 $('#js-help-centre-panel').removeClass('isActive');
                 helpBool = false;
            }
            $('.notification-centre-panel, .nav-notification-centre-btn').addClass('isActive');
            sorBool = true;
            if($scope.shoutOutsRecieved.length < 1){
                recievedService.getSendersOfShoutouts($scope.$parent.user._id).then(function(res) {
                    for(var i = 0; i < res.data.data.length; i++){
                        $scope.shoutOutsRecieved.push(res.data.data[i]);
                    }
                    console.log($scope.shoutOutsRecieved);
                }, function(){
                    alert('There was an error sending your shoutout, please try again later');
                });
            }
        }else {
            $('.notification-centre-panel, .nav-notification-centre-btn').removeClass('isActive');
            sorBool = false;
        }
    }

    //  Get listeners
    $scope.current = 0;
    $scope.getSomeListeners = function(ofs){
        userService.getUsersData(ofs).then(function(result) {
            for(var i = 0; i < result.data.data.length; i++){
                if(result.data.data[i]['_id'] != $scope.$parent.user['_id']){
                    $scope.individulals.push(result.data.data[i]);
                }
            }
            for(var i = 0; i < $scope.individulals.length; i++){
                if($scope.checkShoutsSent($scope.individulals[i]['_id'])){
                    $scope.individulals[i]['shouted'] = 'true';
                }
            }
            $scope.setUpSwipe(0);
        }, function(data, status){
                console.log(status);
                if (status == 401) {
                    var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                    }};
                    $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.user, config)
                    .success(function(data){
                        $scope.user['token'] = data['token'];
                         $scope.updateMyProfile($scope.user);
                    })
                    .error(function(data){
                         $location.path('/#');
                    });
                }
            });
        }

    //  Check whether the user has already been shouted out
    $scope.checkShoutsSent = function(id){
        for(var i = 0; i < $scope.$parent.user.shoutouts_s.length; i++){
            if(id == $scope.$parent.user.shoutouts_s[i]){
                return true;
            }
        }
    }

    //  Add more listeners
    $scope.getMoreListeners = function(a , ap){
        a++;
        ofs = a;
        userService.getUsersData(ofs)
        .then(function(r) {
            for(var i = 0; i < r.data.data.length; i++){
                if(r.data.data[i]['_id'] != $scope.user['_id']){
                    $scope.individulals.push(r.data.data[i]);
                }
                if($scope.checkShoutsSent(r.data.data[i]['_id'])){
                    r.data.data[i]['shouted'] = 'true';
                }
            }
            $scope.setUpSwipe(ap);
        },function(data){

        });
    }

    //  Shout out functionality
    $scope.checkShout = function(number , index){
        $scope.$parent.shoutOutIndex = $scope.individulals[index];
        $timeout(function() {
            $scope.$apply();
            $('.dialog').addClass('active');
        },50);
    }

    //  SWIPES BETWEEN PROFILES
    $scope.setUpSwipe = function(ap){
        for(var i = 0; i < $scope.individulals.length; i ++){
              $scope.individulals[i]['peer'] = $scope.individulals[i].shoutouts_r.length;
        }
        
        //  Build Carousel
        owlBuild = $timeout (
            function () {
                $owls = $('.listeners-profile');
                $owls.owlCarousel({
                    items : 1,
                    margin : 0,
                    autoHeight : true,
                    dots : false,
                    callbacks : true,
                    info : true,
                    startPosition : ap,
                });
                
                //  Remopve preloader and 0 opacity after rebuilding the carousel
                loadTimer = $timeout(function(){
                    $('.listeners-profile').removeClass('isHidden');
                    $('.listener-count').removeClass('loading');
                    $('.listeners-preloader').remove();
                    $('.loading-hint').remove();
                },1500);
                
                //  Check whether we are at the end of the carousel and then get more results
                $owls.on('dragged.owl.carousel', function(e){
                    var addPoss = e.item.index;
                    var countPos = e.item.count;
                    if(addPoss === countPos -1){
                        thisUser = $('.profile:last-of-type').data('me');
                        if(thisUser != lastUser){
                            $('.listeners-profile').addClass('isHidden');
                            $('.listeners').append('<div class="listeners-preloader"><div class="preloader"></div></div>');
                            lastUser = $('.profile:last-of-type').data('me');
                            owlTimer = $timeout(function(){
                                $owls.trigger ('destroy.owl.carousel');
                                $('.listener-profile-li').unwrap();
                                $('.listeners-profile').removeClass('owl-carousel owl-loaded owl-theme');
                                $scope.getMoreListeners(ofs, addPoss);
                            },300);
                        }else {
                            if(alerted === false){
                                alerted = true;
                                alert('you have reached the end of the list!');
                            }
                        }
                    }
                });
            },
            50
        );
        
        myShoutsService.getShouts()
            .success(function(d){
                if(d['data']['shoutouts_r'].length > $scope.$parent.user.shoutouts_r.length){
                    $scope.$parent.user.shoutouts_r = d['data']['shoutouts_r'];
                    $scope.$parent.user.peer = d['data']['shoutouts_r'].length;
                    $scope.notifyMe(d['data']['shoutouts_r'].length);
                }
            })
            .error(function(d){
                console.log('There was an error!');
                $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.user, config)
                .success(function(data){
                    $scope.user['token'] = data['token'];
                    $scope.getProfile($scope.user);
                })
                .error(function(data){
                    console.log(data);
                });
            });

        $scope.notifyMe = function(number){
            $('.nav-notification-inner').addClass('blink');
            $('#js-nav-footer-count').html(number);
            console.log('You now have ' + number + ' shoutouts!');
            $timeout(function(){
                $('.nav-notification-inner').removeClass('blink');
            },4000);
        }
    }
    $scope.$on('$destroy', function(event){
        if(typeof $owls === 'undefined' && $owls != null){
            $owls.trigger ('destroy.owl.carousel');
        }
        if(typeof owlTimer === 'undefined'){
            $timeout.cancel( owlTimer );
        }
        if(typeof loadTimer === 'undefined'){
            $timeout.cancel( loadTimer );
        }
        if(typeof owlBuild === 'undefined'){
            $timeout.cancel( owlBuild );
        }
    });

    //  First function to fire !!!
    $scope.updateMyProfile();
}