function LockedinCtrl($scope, $http, userService, dataService, countService, $routeParams, $location, localStorageService, profileService, $timeout, socket){
    
    $scope.listeners = null;
    $scope.shoutOutSend;
    $scope.shoutOutIndex;
    $owls = null;
    $scope.individulals = [];
    var offset = 1;
    
    //
    //  Get profile from local storage or re-direct
    //
    if($scope.user['token'] === null ){
        $scope.$parent.user = localStorageService.get('user');
        $scope.user = localStorageService.get('user');
        $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
        $scope.$parent.getProfile($scope.$parent.user);
        if($scope.user['token'] === null){
            $location.path('/#');
        }else{
            // $scope.profile = localStorageService.get('profile');
            // $scope.confirmation = localStorageService.get('confirmation');
            // $scope.networks = $scope.profile.user.profile;
        }
    }

    //
    //  Count lockedIn users
    //
    countService.countUsers().then(function(result){
        $scope.count = result.data.data;
    });

    //
    //  Get listeners
    //
    $scope.current = 0;
    $scope.getSomeListeners = function(ofs){

        userService.getUsersData(offset).then(function(result) {
            for(var i = 0; i < result.data.data.length; i++){
                if(result.data.data[i]['_id'] != $scope.$parent.user['_id']){
                    $scope.individulals.push(result.data.data[i]);
                }
            }
            for(var i = 0; i < $scope.individulals.length; i++){
                // console.log('parent user id = ' + $scope.$parent.user['_id']);
                // console.log('this id = ' + $scope.individulals[i]['_id']);
                //console.log($scope.individulals[i]);
                if($scope.checkShoutsSent($scope.individulals[i]['_id'])){
                    $scope.individulals[i]['shouted'] = 'true';
                }
            }

            offset = ofs + 1;
            $scope.setUpSwipe(0);
        }, function(data){
            console.log(data);
        });
    }
    
    //
    //  First call to db for listeners
    //
    $scope.getSomeListeners(1);

    //
    //  Check whether the user has already been shouted out
    //
    $scope.checkShoutsSent = function(id){
        for(var i = 0; i < $scope.$parent.user.shoutouts_s.length; i++){
            if(id == $scope.$parent.user.shoutouts_s[i]){
                return true;
            }
        }
    }

    //
    //  Add more listeners
    //
    $scope.getMoreListeners = function(ofs , ap){
        // if($scope.individulals.length > 1){
        //     for (var i = 0; i < ($scope.individulals.length - 1); i++ ){
        //         $scope.individulals.splice( i, 2 );
        //     }
        // }
        
        userService.getUsersData(1).then(function(r) {
            for(var i = 0; i < r.data.data.length; i++){
                if(r.data.data[i]['_id'] != $scope.user['_id']){
                    $scope.individulals.push(r.data.data[i]);
                }
                if($scope.checkShoutsSent(r.data.data[i]['_id'])){
                    r.data.data[i]['shouted'] = 'true';
                }
            }
            
            $owls = $('.listeners-profile');
            
            $scope.setUpSwipe(ap);
            //
            /////////////////////////////    HEY JORDI! UNCOMMENT THIS SHIZZLE.
            //

            //offset = ofs + 1;
        }, function(data){
            console.log(data);
        });
    }

    //
    //  Shout out functinoality
    //
    $scope.checkShout = function(number , index){
        $scope.$parent.shoutOutIndex = $scope.individulals[index];
        $timeout(function() {
            $scope.$apply();
            $('.dialog').addClass('active');
        },50);
    }

    //
    //  SWIPES BETWEEN PROFILES
    //
    $scope.setUpSwipe = function(ap){
        for(var i = 0; i < $scope.individulals.length; i ++){
              $scope.individulals[i]['peer'] = $scope.individulals[i].shoutouts_r.length;
        }
        
        //
        //  Build Carousel
        //
        $timeout (
            function () {
                $owls = $('.listeners-profile');
                $owls.owlCarousel({
                    items : 1,
                    margin : 0,
                    autoHeight : true,
                    dots : false,
                    callbacks : true,
                    info : true,
                    startPosition : ap,
                });
                
                //
                //  Remopve preloader and 0 opacity after rebuilding the carousel
                //
                $timeout(function(){
                    $('.listeners-profile').removeClass('isHidden');
                    $('.listeners-preloader').remove();
                },500);
                
                //
                //  Check whether we are at the end of the carousel and then get more results
                //
                $owls.on('dragged.owl.carousel', function(e){

                    var addPoss = e.item.index;
                    var countPos = e.item.count;
                    if(addPoss === countPos -1){
                        $('.listeners-profile').addClass('isHidden');
                        $('.listeners').append('<div class="listeners-preloader"><img src="assets/css/img/Preloader_10.gif"</div>');
                        $timeout(function(){
                            //
                            /////////////////////////////    HEY JORDI! UNCOMMENT THIS SHIZZLE.
                            //
                            // addPoss = addPoss 1;
                            $owls.trigger ('destroy.owl.carousel');
                            $('.listener-profile-li').unwrap();
                            $('.listeners-profile').removeClass('owl-carousel owl-loaded owl-theme');
                            $scope.getMoreListeners(offset, addPoss);
                        },200);
                    }
                });
            },
            50
        );
    }

    //
    //  Shout out with sockets io
    //
    // socket.on('identification', function (data) {
    //     $scope.user.preferences['socketioID'] = data.data;
    //     console.log($scope.user);
        // profileService.updatePic($scope.user).then(function(result) {
        //     localStorageService.set('user',$scope.user);
        // }, function(err){
        //     console.log(err);
        // });
    //});
    // socket.on('privateShoutout', function (data) {
    //     console.log(data);
    //     $scope.notifyMe();
    // });

}