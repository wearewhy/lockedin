function dialogCtrl($scope, $routeParams, $location, dataService, $timeout){
    $scope.location = $location.path();
    var dialogTimer;
    $scope.$on('dialogChange', function(){
        if($scope.$parent.deleteToggle == true){
            $scope.dialogBox = 'assets/partials/delete.html';
            var dialogTimer = $timeout(function(){
                 $('.dialog').addClass('active');
             },200);
        }else {
            $scope.dialogBox = 'assets/partials/warning.html';
        }
    });

    $scope.$on('$routeChangeStart', function() { 
        // var ls = localStorageService.get('user');
        // var ts = localStorageService.get('token');
        $scope.location = $location.path();
        switch($scope.location){
            case '/profile' : 
            $scope.dialogBox =  'assets/partials/warning.html';
            break;
            case '/lockedin' : 
            $scope.dialogBox = 'assets/partials/shoutout.html';
            break;
        }
    });

    $scope.getLockedIn = function(){
        $scope.$parent.termsAccepted = true;
        $location.path('/lockedin');
    }
    $scope.declineLockin = function(){
        $('.dialog').removeClass('active');
    }
    $scope.notifyMe = function(){
        $('.nav-notification-inner').addClass('blink');
    }

    //$scope.shoutOut = function(number, socketId){
    $scope.shoutOut = function(number){
        // var s = socketId;
        // console.log('Shout out = ' + socketId);
        params = {
          "id": number
        }
        dataService.sendShoutOut(params).then(function(res) {
            $('.dialog').removeClass('active');
            // $scope.shoutOutNotify(number)
        }, function(){
            alert('There was an error sending your shoutout, please try again later');
        });

        $scope.shoutOutNotify = function(s){
            //console.log(s);
            //socket.emit('newShoutOut', { data : "Hey There!", idTo : s });
        }
        //$scope.shoutOutNotify(s);
    }
      $scope.$on('$destroy', function(event){
        $timeout.cancel( dialogTimer );
    });


}