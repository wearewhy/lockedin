function ProfileCtrl($scope, $rootScope, $routeParams, $timeout, $location, $parse, profileService, deleteUserService, logInService){

    //  Templates
    $scope.instagramTemplate = 'assets/partials/instagram.html';
    $scope.fbMusicTemplate = 'assets/partials/fbMusicTemplate.html';
    var picBool = true, dialogTimer, timer3;
    $scope.user = $scope.$parent.user;
    if($scope.user['token'] === null ){
        if($scope.user['token'] === null){
            $location.path('/#');
        }else{
            var timer = $timeout (
                function () {
                    $scope.setUpProfile();
                },
                50
            );
        }
    }
    else{
        var timer = $timeout (
            function () {
                $scope.setUpProfile();
            },
            50
        );
    }

    // Update Instagram
    if($scope.user.instagram != 'undefined' && $scope.user.instagram != null) {
        $.ajax({
            url:"https://api.instagram.com/v1/users/" + $scope.user.instagram.id + "/media/recent/?client_id=853236a03e36460a882be48ca895a0a6",
            "method": "GET",
            dataType:'jsonp',
            cache:false,
            success: function(data){
                if(data.meta.code == 400){
                        alert('Error updating instagram images');
                }
                if(data.meta.code == 200){
                        $scope.user.instagram.photos = data;
                        $scope.$parent.user.instagram.photos = data;
                }
            }
        });
    }

    //  Profile Images object
    $scope.setUpProfile = function(){
        if(typeof($owls) != 'undefined' || $('.profile-swipe-container').hasClass('owl-carousel')){
            $owls.trigger ('destroy.owl.carousel');
            $('.owl-item').unwrap();
            $('.profile-swipe-container').removeClass('owl-carousel owl-loaded owl-theme');
        }
        $scope.profilePics = [];
        $scope.picPref;
        if(typeof($scope.user.facebook) != 'undefined' && $scope.user.facebook != null){
            $scope.profilePics.push($scope.user.facebook.picture);
        }
        if(typeof($scope.user.twitter) != 'undefined' && $scope.user.twitter != null){
            $scope.profilePics.push($scope.user.twitter.profile_image_url);
        }
        if(typeof($scope.user.instagram) != 'undefined' && $scope.user.instagram != null){
            $scope.profilePics.push($scope.user.instagram.data.profile_picture);
        }
        for (var i = 0; i < $scope.profilePics.length; i++){
            if ($scope.profilePics[i] === $scope.user.preferences.image){
                $scope.picPref = i;
            }
        }
        
        var timerUP = $timeout (
            function () {
                if($scope.profilePics.length > 1 && picBool === true){
                    picBool = false;
                    $owls = $('.profile-pics');
                    $timeout(function(){
                        $owls.owlCarousel({
                            items : 1,
                            margin : 0,
                            dots : false,
                            startPosition : $scope.picPref,
                            loop : true
                        });
                    },400); 
                }else{
                    $owls = $('.profile-pics');
                    $owls.owlCarousel({
                        items : 1,
                        margin : 0,
                        dots : false,
                    });
                }
            },300);
    };

    //  watch for new image and update carousel
    $scope.$on('userUpdated', function() {        $scope.setUpProfile();
    });
    
    //  Warning Dialog
    $scope.lockIn = function(){
        if($scope.termsAccepted === false){
            $scope.$parent.deleteToggle = false;
            $rootScope.$broadcast('dialogChange');
            var dialogTimer = $timeout(function(){
                $('.dialog').addClass('active');
            },200);
            $scope.user.preferences.image = $('.owl-item.active').children().children('.profile-pic').data('img');
        }
        else {
            $location.path('/lockedin');
        }
    };

    //  Remove network from profile
    $scope.remove = function(network) {
        $scope.user[network] = null;
        $scope.$parent.user[network] = null;
        profileService.updatePic($scope.user)
        .then(function(data){
            console.log(data);
        }, function(data){
            alert('Error : Could not update profile');
        });
    };

    //  Delete profile
    $scope.deleteProfile = function(){
        $scope.$parent.deleteToggle = true;
        $rootScope.$broadcast('dialogChange');
    };
    $(document).on('click', '.deleteBtn', function(){
        deleteUserService.del()
            // .success(function(data){
            //     console.log(data);
            // })
            .then(function(){
                $('.dialog').removeClass('active');
                logOutSocial();
                $location.path('/login');
            }, function(data){
                alert(data);
            });
    });

    function logOutSocial(){
        var fb = hello( "facebook" ).getAuthResponse();
        var tw = hello( "twitter" ).getAuthResponse();
        var inst = hello( "instagram" ).getAuthResponse();

        if(fb != null){
            hello( "facebook" ).logout({force:true},function(){
                logOutSocial();
            });
        }
        if(tw != null){
            hello( "twitter" ).logout(function(){
                tw.access_token = null;
                logOutSocial();
            });
        }
        else if(tw != (inst)){
            hello( "instagram" ).logout(function(){
                logOutSocial();
            });
        }
        else {
            $location.path("/login");
        }
    }

    $scope.$on('$destroy', function(event){
        if(timer){
            $timeout.cancel( timer );
        }
        if(dialogTimer){
            $timeout.cancel( dialogTimer );
        }
        if(timer3){
            $timeout.cancel( timer3 );
        }
    });
}