angular.module('globalModule', [ 'ngTouch'])
    .controller('GlobalCtrl', [
        '$scope',
        '$http',
        '$location',
        '$timeout',
        '$rootScope',
        'leaveService',
        'logInService',
        function($scope, $http, $location, $timeout, $rootScope, leaveService, logInService) {
            $scope.user = {
                facebook : null,
                 twitter : null,
               instagram : null,
             preferences : {},
                   token : null,
            };
            $scope.name;
            $scope.shoutOutIndex;
            $scope.termsAccepted = false;
            $scope.notificationsCentre = 'assets/partials/notificationsCentre.html';
            $scope.helpCentre = 'assets/partials/helpCentre.html';

            //
            //  CHECK SOCIAL LOG IN STATUS
            //
            var online = function(session){
                var current_time = (new Date()).getTime() / 1000;
                return session && session.access_token && session.expires > current_time;
            };
            console.log('============ CHECK ============');
            var fb = hello( "facebook" ).getAuthResponse();
            var tw = hello( "twitter" ).getAuthResponse();
            if(tw === null && fb === null){
                $location.path("/login");
            }
            
            //
            //  Check whether this is a new profile or an existing user
            //
            $scope.getProfile = function(user){
                $scope.user = user;
                $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
                var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                    }
                };
                 $http.get('http://lockedinapi.herokuapp.com/user', config)
                .success(function(data){
                    $timeout(function() {
                        $scope.user = data['data'];
                        $scope.user['token'] = data['data']['access_token'];
                        $scope.user['peer'] = $scope.user.shoutouts_r.length;
                        if($scope.user['twitter'] != null){
                            $scope.user['twitter']['profile_image_url'] =  $scope.user['twitter']['profile_image_url'].replace(/_normal/g, "");
                        }
                        // localStorageService.set('user', $scope.user);
                        $rootScope.$broadcast('userUpdated', $scope.user);
                        $location.path("/profile");
                    }); 
                })
                .error(function(data, status){
                    console.log(status);
                    if (status == 401) {
                        var config = {headers:  {
                        'Authorization' : 'Bearer ' + $scope.user['token']
                            }
                        };
                        $http.post('http://lockedinapi.herokuapp.com/user/reftoken', $scope.user, config)
                        .success(function(data){
                            $scope.user['token'] = data['token'];
                            $scope.getProfile($scope.user);
                        })
                        .error(function(data){
                            console.log(data);
                        });
                    }
                    if(status == 400){
                        $location.path("/login");
                    }
                }); 
            }

            //
            //  Send login credentials and initial social media data to server
            //
            $scope.sendUser = function(user, profileSize){
                logInService.logInData(user)  
                .success(function(data){
                    $scope.user['token'] = data.token;
                    $scope.user['id'] = data['_id'];
                    $http.defaults.headers.common.Authorization='Bearer ' + $scope.user['token'];
                    $scope.getProfile($scope.user);
                })
                .error(function(data){
                    console.log(data);
                });
            }

            //
            //  For adding aditional social media to profile
            //
            $scope.mergeAccounts = function(user){
                $http.post('http://lockedinapi.herokuapp.com/user/merge', user)
                .success(function(data){
                    $timeout(function() {
                        $location.path("/profile");
                        $scope.$apply();
                    },400);
                })
                .error(function(data){
                    console.log(data);
                });
            }

            //
            //  OAuth Variables
            //
            hello.init({
                facebook : '244491825745560',
                 twitter : 'LweKMQHRSVY0Wqbxt6uvGiyVF',
               instagram : '853236a03e36460a882be48ca895a0a6',
                  google : '862506358537-1rkstooaht2ao5gig6llug776v3m0alf.apps.googleusercontent.com'
             },{
                oauth_proxy : 'https://auth-server.herokuapp.com/proxy'
            });

            //
            //  Initial Login and profile creation
            //
            $scope.login = function(network, profileSize){
                // localStorageService.clearAll();
                // window.localStorage.clear();
                // localStorageService.cookie.remove('user');
                // localStorageService.cookie.clearAll();
                $('.social-loading').css({'display':'block'});
                hello.login( network, {scope:'friends, basic, user_likes'}, function(e){
                    hello( network ).api("me").success(function(json){
                        $scope.user['social'] = network;
                        $scope.user[network] = json;
                        $scope.name = $scope.user[network].name;
                        $scope.$apply();
                        if(network === 'facebook'){
                            $scope.user[network].picture =  $scope.user[network].picture + '?type=large';
                            hello( network ).api(network+':me/music').success(function(json){
                                if(json){
                                    $scope.user.facebook['music'] = json;
                                    if(profileSize === 1){
                                        $http.defaults.headers.common.Authorization= null;
                                        $scope.user['token'] = null;
                                        $scope.sendUser($scope.user, profileSize);
                                    }else{
                                       $scope.mergeAccounts($scope.user);
                                    } 
                                }
                                //localStorageService.set('user', $scope.user);
                            }).error(function(e){
                                console.log(e);
                            });
                        }
                        else if(network === 'instagram'){
                            hello( network ).api(network+':me/photos').success(function(json){
                                if(json){
                                    $scope.user.instagram['photos'] = json;
                                    if(profileSize === 1){
                                        $http.defaults.headers.common.Authorization= null;
                                        $scope.user['token'] = null;
                                       $scope.sendUser($scope.user, profileSize);
                                    }else{
                                       $scope.mergeAccounts($scope.user);
                                    } 
                                }
                                //localStorageService.set('user', $scope.user);
                            }).error(function(e){
                                console.log(e);
                            });
                        }
                        else{
                            // localStorageService.set('user', $scope.user);
                             if(profileSize === 1){
                                $http.defaults.headers.common.Authorization= null;
                                $scope.user['token'] = null;
                                $scope.sendUser($scope.user, profileSize);
                            }else{
                               $scope.mergeAccounts($scope.user);
                            } 
                        }
                    }).error(function(){
                        alert("Authentication error. Please try again");
                         $('.social-loading').css({'display':'none'});
                    });
                });   
            }

            //
            //  Logout of app / network * Twitter offers no network logout
            //
            $scope.logout = function(){
                leaveService.leaveApp().then(function(result){
                    function logOutSocial(){
                        var fb = hello( "facebook" ).getAuthResponse();
                        var tw = hello( "twitter" ).getAuthResponse();
                        var inst = hello( "instagram" ).getAuthResponse();

                        if(fb != null){
                            hello( "facebook" ).logout({force:true},function(){
                                logOutSocial();
                            });
                        }
                        if(tw != null){
                            hello( "twitter" ).logout(function(){
                                tw.access_token = null;
                                logOutSocial();
                            });
                        }
                        else if(online(inst)){
                            hello( "instagram" ).logout(function(){
                                logOutSocial();
                            });
                        }
                        else {
                            $location.path("/login");
                            //  localStorageService.clearAll();
                            // window.localStorage.clear();
                            // localStorageService.cookie.remove('user');
                        }
                    }
                    logOutSocial();
                    
                },function(data){
                    console.log(data);
                });
            }

            //
            //  Delete dialog toggle
            //
            $scope.deleteToggle = false;

}]);