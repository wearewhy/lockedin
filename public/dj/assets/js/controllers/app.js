angular.module('AppCtrl', ['ngRoute', 'getData'])
    .config(siteRouter);

function siteRouter($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'assets/partials/login.html',
            controller : 'formCtrl'
        })
        .when('/console', {
            templateUrl : 'assets/partials/console.html',
            controller : 'ConsoleCtrl'
        });
}