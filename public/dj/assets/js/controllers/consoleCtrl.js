function ConsoleCtrl($scope, $http, userService, $location, dataService, $timeout){
    var clickedElement, params, offset = 0;
    $scope.listenersArray = [];

    // Redirect if no token
    if(! $scope.$parent.user.token){
        $location.path('/')
    }

    // Check for class
    function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    // Set global header
    var config = {headers:  {
            'Authorization' : 'Bearer ' + $scope.$parent.user['token']
        }
    };
    $http.defaults.headers.common.Authorization='Bearer ' + $scope.$parent.user['token'];

    // Remove DJs from list and get length of shoutouts list
    $scope.organiseListeners = function(ar){
        //console.log(offset);
        for(var i = 0; i < ar.length; i++){
            if(ar[i].preferences.is_dj === false){
                $scope.listenersArray.push(ar[i]);
            }
            if(i === ar.length -1){
                $scope.highlightBoxes();
            }
        }
    }
    $scope.highlightBoxes = function(){
        $timeout(function(){
            for(var i = 0; i < $scope.listenersArray.length; i++){
                $scope.listenersArray[i]['shoutCount'] = $scope.listenersArray[i].shoutouts_r.length;
                for(var dj = 0; dj < $scope.listenersArray[i].shoutouts_dj.length; dj++){
                    if($scope.listenersArray[i].shoutouts_dj[dj] === $scope.$parent.user.initials){
                        var selecta = $scope.listenersArray[i]['_id']; 
                        $('#js-'+ selecta).addClass('isClicked');
                    }
                }
            }
        },50);
    }

    // Send shoutout
    $scope.shoutout = function($event, id, index){
        clickedElement = $event.currentTarget;
        //console.log(index);
        if(!hasClass(clickedElement, 'isClicked')){
            clickedElement.className += ' isClicked';
            $scope.listenersArray[index].shoutCount += 1;
            $scope.listenersArray[index].shoutouts_dj.push($scope.$parent.user.initials);
            var currentdate = new Date();
            var mins = ('0'+currentdate.getMinutes()).slice(-2);
            var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + mins;
            console.log(datetime);
            params = {
              "id": id,
              "initials" : $scope.$parent.user.initials,
              "when" : datetime
            }
            dataService.sendShoutOut(params)
                .success(function(res) {
                   // console.log(res);
                }) 
                .error(function(res){
                    console.log(res);     
            });
        }
    }

    // Refresh Console
    $scope.refreshConsole = function(){
        offset = 1;
        userService.getUsersData(offset)
        .success(function(d){
            $scope.listenersArray = [];
            $scope.organiseListeners(d.data);
        })
        .error(function(d){
            console.log(d);
        });
    }
    //Get more users 
    $scope.getMoreUsers = function(){
        offset += 1; 
        userService.getUsersData(offset)
        .success(function(d){
            console.log(d);
            $scope.organiseListeners(d.data);
        })
        .error(function(d){
            console.log(d);
        });
    }

    // Load first set of lsieners
    $scope.getMoreUsers();
}