function formCtrl($scope, $http, signUpService, signInService, $location){

    var data, ini;
    $scope.loginTemplete = "assets/partials/returndj.html";
    $scope.user = {
        name : null,
        token : null,
    }
    $scope.loginTempleteChange = function(num){
        $('#js-login-nav-return , #js-login-nav-new').toggleClass('current');
        if(num === 1) {
            $scope.loginTemplete = "assets/partials/returndj.html";
        }else {
            $scope.loginTemplete = "assets/partials/newdj.html";
        }
    }
    
    $scope.newDj = function(){
        $scope.$parent.user.initials = $("#js-initials").val();
        data = $("#registerForm").serializeArray();

        signUpService.djData(data)
            .success(function(e){
                $scope.$parent.user['token'] = e.token;
                $location.path('/console');
            })
            .error(function(e){
                console.log(e);
            });
    }
    $scope.djLogin = function(){
        var data = $("#signUpForm").serializeArray()
        signInService.djSignIn(data)
            .success(function(d){
                $scope.$parent.user['token'] = d.token;
                $scope.$parent.user.name = d.user.preferences.name;
                $scope.$parent.user.initials = d.user.preferences.initials;
                //console.log($scope.$parent.user.initials);
                // console.log(d.user);
                $location.path('/console');
            })
            .error(function(d){
                console.log('error ' + d);
            });
    }

}