angular.module('getData', ['ngResource'])
    .service('signUpService', ['$http', function ($http){
        this.djData = function(user){
            return $http.post('http://lockedinapi.herokuapp.com/dj/register/', user);
        }
    }])
    .service('signInService', ['$http', function ($http){
        this.djSignIn = function(user){
            return $http.post('http://lockedinapi.herokuapp.com/dj/login/', user);
        }
    }])
    .service('userService', ['$http', function ($http) {
        this.getUsersData = function (offset) {
            return $http.get('http://lockedinapi.herokuapp.com/users/all/' + offset);
        };
    }])
    .service('dataService', ['$http', function ($http) {
        this.sendShoutOut = function (params) {
            //console.log(params);
            return $http.post('http://lockedinapi.herokuapp.com/djshoutout', params);
        };
    }]);